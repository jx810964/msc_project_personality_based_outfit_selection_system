#https://ieeexplore.ieee.org/document/9025572
#https://ieeexplore.ieee.org/document/8265477
#https://ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=outfit%20recommend
#https://ieeexplore.ieee.org/document/9804781


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 19:50:42 2024

@author: shrutipatkar
"""

import pandas as pd
import numpy as np
#pip install country_converter --upgrade
#import country_converter as coco
import difflib
import pycountry
from geopy.geocoders import Nominatim
import re
from datetime import datetime
from matplotlib import colors
from spellchecker import SpellChecker
from sklearn.model_selection import train_test_split

spell = SpellChecker()

df = pd.read_excel("dataset.xlsx")

df.drop(columns=["Timestamp", "Consent Form:\nI have read the description and accompanying Information Sheet relating to the project on Personality Based Outfit Selection System and understand that participation is entirely voluntary and that I have the right to withdraw from the project any time, and that this will be without detriment. This application has been reviewed by the University Research Ethics Committee and has been given a favourable ethical opinion for conduct.", "Please read the document carefully and proceed with th consent form."], inplace=True)


# dictionary to map the current column names to new names
new_column_names = {
    'Country of Birth': 'Country_of_Birth',
    'Country of Residence': 'Country_of_Residence',
    'Birthdate': 'Birthdate',
    'Gender': 'Gender',
    'Approximate weight in kilogram': 'Weight_kg',
    'Approximate height in centimetre': 'Height_cm',
    'What type of clothes you prefer to wear': 'Preferred_Clothing_Type',
    'How do you define yourself': 'Personality',
    'Size of clothes you wear': 'Clothing_Size',
    'What is your favourite color.': 'Favourite_Color',
    'Do you think this color looks best on you and boost your confidence?': 'Color_Boost_Confidence',
    'Which color do you think looks best on you and makes you feel confident?': 'Best_Color_Confidence',
    'What type of clothing you like to wear': 'Preferred_Clothing',
    'What is your preferred bottoms': 'Preferred_Bottoms',
    'Material preferred for your bottoms?': 'Bottoms_Material',
    'Length of bottoms you prefer?': 'Bottoms_Length',
    'How do you prefer the fitting of your bottoms?': 'Bottoms_Fitting',
    'Which color of bottoms would you prefer. (Please mention two colours)': 'Bottoms_Color',
    'Any other specific detail about the bottoms you will like to mention?': 'Bottoms_Other_Details',
    'What is your prefered upperwear on the selected bottoms?': 'Preferred_Upperwear',
    'What material you will prefer for the upperwear?': 'Upperwear_Material',
    'What length of upperwear would you choose?': 'Upperwear_Length',
    'What is your prefered neckline for them?': 'Upperwear_Neckline',
    'What is your preferred sleeve type?': 'Upperwear_Sleeve_Type',
    'What pattern will you like on you upperwear?': 'Upperwear_Pattern',
    'which type of print': 'Upperwear_Print_Type',
    'What is your preferred color for the selected upperwear to go on the selected bottoms?Please select two)': 'Upperwear_Color',
    'Any other specific detail about the upper wear?': 'Upperwear_Other_Details',
    'Favourite type of single piece wear': 'Favourite_Single_Piece',
    'What is your preferred material?': 'Single_Piece_Material',
    'How do you prefer the fitting?': 'Single_Piece_Fitting',
    'What is your preferred length?': 'Single_Piece_Length',
    'How will you prefer your sleeves?': 'Single_Piece_Sleeves',
    'How would you prefer your neckline?': 'Single_Piece_Neckline',
    'What is your preferred pattern?': 'Single_Piece_Pattern',
    'which type of print.1': 'Single_Piece_Print_Type',
    'Would you prefer the colour which you have mentioned in previous section?': 'Single_Piece_Preferred_Color',
    'What color would you like to choose for you single piece wear': 'Single_Piece_Color',
    'Any other specific detail about the clothing?': 'Clothing_Other_Details',
    'Do you like to wear outer layer on top?': 'Outer_Layer_Preference',
    'What type of outer layer you prefer to wear': 'Preferred_Outer_Layer',
    'What is your preferred material?.1': 'Outer_Layer_Material',
    'How do you prefer the fitting?.1': 'Outer_Layer_Fitting',
    'What is your preferred length?.1': 'Outer_Layer_Length',
    'How will you prefer your sleeves?.1': 'Outer_Layer_Sleeves',
    'How would you prefer your neckline?.1': 'Outer_Layer_Neckline',
    'Type of closing ': 'Outer_Layer_Closing_Type',
    'What color would you like to choose?': 'Outer_Layer_Color',
    'What is your preferred pattern?.1': 'Outer_Layer_Pattern',
    'which type of print.2': 'Outer_Layer_Print_Type',
    'Any other specific detail about the outer layer?': 'Outer_Layer_Other_Details'
}

# Rename the columns in the DataFrame
df.rename(columns=new_column_names, inplace=True)

df.loc[df['Country_of_Residence'] == 'Same as Country of Birth', 'Country_of_Residence'] = df['Country_of_Birth']
df.loc[df['Color_Boost_Confidence'] == 'Yes', 'Best_Color_Confidence'] = df['Favourite_Color']


df["Color_Boost_Confidence"].replace({"Yes":1, "No":0}, inplace=True)
df["Country_of_Birth"] = df["Country_of_Birth"].astype(str)
df["Country_of_Residence"] = df["Country_of_Residence"].astype(str)   
df["Country_of_Birth"] = df["Country_of_Birth"].str.strip().str.title()
df["Country_of_Residence"] = df["Country_of_Residence"].str.strip().str.title()

# Create a list of official country names
country_names = [country.name for country in pycountry.countries]
alpha_2_mapping = {country.alpha_2.title(): country.name.title() for country in pycountry.countries}

df["Country_of_Birth"] = df["Country_of_Birth"].apply(lambda x: re.sub(r'[^a-zA-Z\s]', '', x) if isinstance(x, str) else x)
df["Country_of_Birth"] = df["Country_of_Birth"].apply(lambda x: x.split(",")[-1] if isinstance(x, str) else x)

df["Country_of_Residence"] = df["Country_of_Residence"].apply(lambda x: re.sub(r'[^a-zA-Z\s]', '', x) if isinstance(x, str) else x)
df["Country_of_Residence"] = df["Country_of_Residence"].apply(lambda x: x.split(",")[-1] if isinstance(x, str) else x)


df["Country_of_Birth"] = df["Country_of_Birth"].replace({"Uk":"United Kingdom", 
                                "Usa": "United States",
                                "Us":"United States",
                                "America":"United States",
                                "United States Of America": "United States",
                               "England": "United Kingdom",
                               "Wales":"United Kingdom",
                               "Scotland":"United Kingdom",
                               "Ksa": "Saudi Arabia",
                               "Rsa": "South Africa",
                               "Great Britain":"United Kingdom",
                               "Britain": "United Kingdom",
                               "Uae":"United Arab Emirates",
                               "United Kingdom Of Great Britain And Northern Ireland":"United Kingdom"})

df["Country_of_Residence"] = df["Country_of_Residence"].replace({"Uk":"United Kingdom", 
                                "Usa": "United States",
                                "Us":"United States",
                                "America":"United States",
                                "United States Of America": "United States",
                               "England": "United Kingdom",
                               "Wales":"United Kingdom",
                               "Scotland":"United Kingdom",
                               "Ksa": "Saudi Arabia",
                               "Rsa": "South Africa",
                               "Great Britain":"United Kingdom",
                               "Britain": "United Kingdom",
                               "Uae":"United Arab Emirates",
                               "United Kingdom Of Great Britain And Northern Ireland":"United Kingdom"})


# Replace blank, NaN, or unreadable entries in 'Country_of_Birth' with 'Country_of_Residence'
df["Country_of_Birth"] = df["Country_of_Birth"].apply(lambda x: np.nan if isinstance(x, str) and x.strip() in ['', 'Cannot Read Text'] else x)
df["Country_of_Residence"] = df["Country_of_Residence"].apply(lambda x: np.nan if isinstance(x, str) and x.strip() in ['', 'Cannot Read Text'] else x)

df["Country_of_Birth"].fillna(df["Country_of_Residence"], inplace=True)
df["Country_of_Residence"].fillna(df["Country_of_Birth"], inplace=True)


# Function to find the best match and its confidence
def get_best_match_with_confidence(country):
    print(country)
    try:
        try:
            country = alpha_2_mapping[country]
        except:
            pass
        matches = difflib.get_close_matches(country, country_names, n=1)
        if matches:
            best_match = matches[0]
            confidence = difflib.SequenceMatcher(None, country, best_match).ratio()
            if confidence >= 0.80:
                return best_match, confidence
            else:
                return country, 0
        else:
            return country, 0
    except:
        return country, 0

# Get the unique values from the 'Country_of_Birth' column
unique_countries_1 = list(df['Country_of_Birth'].unique())
unique_countries_2 = list(df['Country_of_Residence'].unique())
unique_countries = np.union1d(unique_countries_1, unique_countries_2)




# Apply the function to the unique values
matched_countries = {country: get_best_match_with_confidence(country) for country in unique_countries}

# Create two new columns in the DataFrame by mapping the results back to the original 'Country_of_Birth' column
df['Country_of_Birth'] = df['Country_of_Birth'].apply(lambda x: matched_countries[x][0] if x in matched_countries else x)
#df['Match_Confidence'] = df['Country_of_Birth'].apply(lambda x: matched_countries[x][1] if x in matched_countries else 0)

df['Country_of_Residence'] = df['Country_of_Residence'].apply(lambda x: matched_countries[x][0] if x in matched_countries else x)

# Create a mapping from country names to numerical values
country_mapping = {country: idx + 1 for idx, country in enumerate(unique_countries)}

# Function to convert country names to numbers
def convert_country_to_number(country):
    return country_mapping.get(country, None)
# Apply the function to the 'Country' column

df['Country_of_Birth'] = df['Country_of_Birth'].apply(convert_country_to_number)
df['Country_of_Residence'] = df['Country_of_Residence'].apply(convert_country_to_number)

# Display the DataFrame with the new columns
#df_2 = df[['Country_of_Birth', 'Matched_Country', 'Match_Confidence']]

#df_2 = df_2[df_2["Match_Confidence"]  < 0.80]

#nan, cannot read text, numbers, date

#df.drop_duplicates(inplace=True)

 
def calculateAge(birthDate):
    try:
        today = datetime.today()
        age = today.year - birthDate.year -((today.month, today.day) <(birthDate.month, birthDate.day))    
    except:
        return None
    return age
     
# Driver code 
df['Birthdate'] = pd.to_datetime(df['Birthdate'], errors='coerce')
df['Age'] = df['Birthdate'].apply(calculateAge)

df['Weight_kg'] = df['Weight_kg'].apply(lambda x: x * 100 if x < 1 else x)
df['Weight_kg'] = df['Weight_kg'].apply(lambda x: x * 10 if x < 10 else x)
df['Height_cm'] = df['Height_cm'].apply(lambda x: x * 100 if x < 2 else x)
df['Height_cm'] = df['Height_cm'].apply(lambda x: x * 30.48 if x < 15 else x)
df['Height_cm'] = df['Height_cm'].apply(lambda x: x * 2.54 if x < 100 else x)


# Function to normalize sizes like "4XL" into "XXXXL" and include "XS"
def normalize_size_large(size):
    match = re.match(r"(\d+)(XL)", size.strip())
    if match:
        number = int(match.group(1))
        return "X" * number + "L"
    else:
        return size.strip()

# Normalize the sizes in the 'Size of clothes you wear' column
df['Clothing_Size'] = df['Clothing_Size'].apply(lambda sizes: ','.join([normalize_size_large(size) for size in sizes.split(',')]))

def normalize_size_small(size):
    match = re.match(r"(\d+)(XS)", size.strip())
    if match:
        number = int(match.group(1))
        return "X" * number + "L"
    else:
        return size.strip()

# Normalize the sizes in the 'Size of clothes you wear' column
df['Clothing_Size'] = df['Clothing_Size'].apply(lambda sizes: ','.join([normalize_size_small(size) for size in sizes.split(',')]))


# Define a mapping from size to a numerical value using the provided values
size_mapping = {'XXXXXS': 1, 'XXXXS': 2, 'XXXS': 3, 'XXS': 4, 'XS': 5, 
                'S': 6, 'M': 7, 'L': 8,
                'XL': 9, 'XXL': 10, 'XXXL': 11, 'XXXXL': 12, 'XXXXXL': 13}

# Function to convert clothes sizes to numerical values
def convert_size_to_numbers(sizes):
    if pd.isna(sizes):
        return 0
    sizes = sizes.split(',')
    numeric_sizes = [size_mapping[size.strip()] for size in sizes if size.strip() in size_mapping]
    return numeric_sizes if numeric_sizes else None

# Apply the function to the 'Size of clothes you wear' column

df['Clothing_Size'] = df['Clothing_Size'].apply(convert_size_to_numbers)

personality_mapping = {
    'Introvert (Are you shy, reticent person?)': 1,
    'Ambivert (Are you able to balance between extrovert and introvert personality traits?)': 2,
    'Extrovert (Are you outgoing, socially confident?)': 3,
}

# Function to convert personality types to numerical values
def convert_personality_to_number(personality):
    for key in personality_mapping:
        if key in personality:
            return personality_mapping[key]
        else:
            return 4

# Apply the function to the 'How do you define yourself' column
df['Personality'] = df['Personality'].apply(convert_personality_to_number)



df['Favourite_Color'] = df['Favourite_Color'].str.split(',')

df = df.explode('Favourite_Color')
df = df.explode("Clothing_Size")

def custom_to_rgba(color_name):
    try:
        color_name_2=[]
        # Preprocess the color name
        for i in color_name.split():
            color_name_2.append(spell.correction(i))
        color_name_2 = ' '.join(map(str, color_name_2))
        color_name_3 = color_name.lower().replace(" ", "")
        try:
            rgba = colors.to_rgba(color_name_3)
        except:
                rgba = colors.to_rgba(color_name_2.split()[-1])
        rgba = (np.array(rgba) * 255).astype(np.uint8)
        int32_value = (rgba[0] << 24) + (rgba[1] << 16) + (rgba[2] << 8) + rgba[3]
        return int32_value
    except :
        print(f"Invalid color name: {color_name}")
        return 0  # Default to transparent if invalid

df["Favourite_Color"] = df["Favourite_Color"].apply(lambda x: re.sub(r'[^a-zA-Z\s]', '', x) if isinstance(x, str) else x)
df['Favourite_Color'] = df['Favourite_Color'].apply(custom_to_rgba)

df['Gender'] = df['Gender'].apply(lambda x: 1 if x.lower() == 'male' else (3 if x.lower() == 'female' else 2))


df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].apply(lambda x:str(x).lower().strip())

def remove_bracketed_text(text):
    if pd.isna(text):
        return text
    return re.sub(r'\(.*?\)', '', text).strip()

# Apply the function to a specific column, e.g., 'Preferred_Clothing_Type'
df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].apply(remove_bracketed_text)


df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].apply(lambda x:x.replace("both", "").replace("combination", "").replace("smart", ""))

df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].str.split('and')
df = df.explode("Preferred_Clothing_Type")
df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].str.split('/')
df = df.explode("Preferred_Clothing_Type")

import nltk
from nltk.corpus import stopwords

# Download NLTK stop words
nltk.download('stopwords')

# Define the stop words
stop_words = set(stopwords.words('english'))

# Function to remove stop words
def remove_stop_words(text):
    if pd.isna(text):
        return text
    words = text.split()
    filtered_words = [word for word in words if word.lower() not in stop_words]
    return ' '.join(filtered_words)

# Apply the function to a specific column, e.g., 'How do you define yourself'
df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].apply(remove_stop_words)

df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].apply(lambda x:x.replace(" ", ""))

allowed_types = ['semiformal', 'casual', 'formal', 'partywear', 'traditional', 'sportswear', 'streetwear']


def replace_clothing_type(text):
    text = text.lower()  # Convert to lowercase for consistent matching
    if 'street' in text:
        return 'streetwear'
    elif 'sport' in text:
        return 'sportswear'
    elif 'dress' in text or 'saree' in text or 'sadi' in text or 'traditional' in text or 'religi' in text:
        return 'traditional'
    elif 'offic' in text:
        return 'formal'
    elif 'athleisure' in text:
        return 'casual'
    elif 'basic' in text:
        return 'casual'
    elif text in allowed_types:
        return text
    else:
        return ', '.join(allowed_types)

# Apply the function to the cleaned column
df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].apply(replace_clothing_type)

df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].str.split(',')

df = df.explode('Preferred_Clothing_Type')

# Create a mapping from clothing types to numerical values
clothing_type_mapping = {clothing_type: idx + 1 for idx, clothing_type in enumerate(allowed_types)}

# Function to convert clothing types to numbers
def convert_clothing_type_to_number(clothing_type):
    if ',' in clothing_type:
        return ', '.join(str(clothing_type_mapping[ct.strip()]) for ct in clothing_type.split(','))
    else:
        return clothing_type_mapping.get(clothing_type, None)

# Apply the function to the final cleaned column
df['Preferred_Clothing_Type'] = df['Preferred_Clothing_Type'].apply(convert_clothing_type_to_number)

# Apply the function to the cleaned column


# Display the first few rows of the updated DataFrame
print(df.head())

####

#results

# Define the mapping for the 'Preferred_clothing' column
preferred_clothing_mapping = {
    'Single Piece Wear': 1,
    'Two Piece Wear': 2
}


# Apply the mapping to convert the 'Preferred_clothing' column to numerical values
df['Preferred_Clothing'] = df['Preferred_Clothing'].map(preferred_clothing_mapping)
    
    
def handle_output_columns(df, column_name):    
    df[column_name] = df[column_name].apply(lambda x:str(x).lower())
    
    df[column_name] = df[column_name].str.split(",")
    df = df.explode(column_name)
    df[column_name] = df[column_name].str.split(" and ")
    df = df.explode(column_name)
    df[column_name] = df[column_name].str.split(" or ")
    df = df.explode(column_name)
    df[column_name] = df[column_name].str.split("/")
    df = df.explode(column_name)
    
    df[column_name] = df[column_name].apply(remove_stop_words)
    
    df[column_name] = df[column_name].apply(lambda x: str(x).split(" ")[-1] if len(str(x).split(" ")) > 1 else x)
    if column_name.find('Color') == -1 or column_name == "Single_Piece_Preferred_Color":
        # Create a mapping from unique values to numerical values
        preferred_bottoms_mapping = {bottom: idx + 1 for idx, bottom in enumerate(df[column_name].unique())}
        preferred_bottoms_mapping[np.nan] = 0 
        print(preferred_bottoms_mapping)
        df[column_name] = df[column_name].map(preferred_bottoms_mapping)
    else:
        df[column_name] = df[column_name].apply(lambda x: re.sub(r'[^a-zA-Z\s]', '', x) if isinstance(x, str) else x)
        df[column_name] = df[column_name].apply(custom_to_rgba)
   
    return df

# Apply the transformation to 'Preferred_Bottoms' column

# List of columns to process
columns_to_process = [
    'Preferred_Bottoms', 'Bottoms_Material', 'Bottoms_Length', 'Bottoms_Fitting', 'Bottoms_Color', 
    'Preferred_Upperwear', 'Upperwear_Material', 'Upperwear_Length', 'Upperwear_Neckline', 'Upperwear_Sleeve_Type', 
    'Upperwear_Pattern', 'Upperwear_Print_Type', 'Upperwear_Color', 'Favourite_Single_Piece', 
    'Single_Piece_Material', 'Single_Piece_Fitting', 'Single_Piece_Length', 'Single_Piece_Sleeves', 'Single_Piece_Neckline', 
    'Single_Piece_Pattern', 'Single_Piece_Print_Type', 'Single_Piece_Preferred_Color', 'Single_Piece_Color',
    'Outer_Layer_Preference', 'Preferred_Outer_Layer', 'Outer_Layer_Material', 
    'Outer_Layer_Fitting', 'Outer_Layer_Length', 'Outer_Layer_Sleeves', 'Outer_Layer_Neckline', 'Outer_Layer_Closing_Type', 
    'Outer_Layer_Color', 'Outer_Layer_Pattern', 'Outer_Layer_Print_Type'
]



# Apply the function to each column in the list
for column in columns_to_process:
    df = handle_output_columns(df, column)

df = df.drop_duplicates()
    
# Merge the columns into one array
df['Merged_Columns'] = df[columns_to_process].apply(lambda row: row.values.tolist(), axis=1)


df= df[['Country_of_Birth', 'Country_of_Residence', 'Gender',
       'Weight_kg', 'Height_cm', 'Preferred_Clothing_Type',
       'Clothing_Size', 'Favourite_Color', 'Color_Boost_Confidence',  'Age',
       'Preferred_Clothing', 'Merged_Columns']]

# Perform train-test split
# Create a combined column for stratification

df.fillna(0, inplace=True)

# Perform stratified train-test split
X = df.drop(columns=['Preferred_Clothing', 'Merged_Columns'])
y = df[['Preferred_Clothing']]



# Perform stratified train-test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, stratify=df['Preferred_Clothing'])
"""

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report


# Apply a logistic regression model
model = LogisticRegression(max_iter=1000)
model.fit(X_train, y_train)

# Make predictions
y_pred = model.predict(X_test)

# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy}")

print("Classification Report:")
print(classification_report(y_test, y_pred))

results_df = pd.DataFrame(y_test)

# Reset index to match y_pred (important if the indices are not aligned)
results_df.reset_index(drop=True, inplace=True)

# Add the predictions to the DataFrame
results_df['Predicted'] = y_pred

# Display the results DataFrame
print(results_df)
"""

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix


tree_model = DecisionTreeClassifier(random_state=42)
tree_model.fit(X_train, y_train)

# Make predictions
y_pred = tree_model.predict(X_test)

# Create a DataFrame to display y_test and y_pred
results_df = pd.DataFrame(y_test)

#results_df = pd.DataFrame({'Actual': y_test, 'Predicted': y_pred})

# Reset the index if necessary (to align the rows correctly)
results_df.reset_index(drop=True, inplace=True)
# Reset index to match y_pred (important if the indices are not aligned)
results_df.reset_index(drop=True, inplace=True)

# Add the predictions to the DataFrame
#results_df['Predicted'] = y_pred


# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy}\n")
print("Classification Report:")
print(classification_report(y_test, y_pred))

# Display the first few rows of the results
print(results_df.head())
cm = confusion_matrix(y_test, y_pred)
print(cm)

tn, fp, fn, tp = cm.ravel()

# Print true positives, false positives, true negatives, false negatives
print(f"True Positives (TP): {tp}")
print(f"False Positives (FP): {fp}")
print(f"True Negatives (TN): {tn}")
print(f"False Negatives (FN): {fn}")

"""
# Convert the color name 'blue' to its RGB representation
ann = colors.to_rgba('darkblue')
ann = np.asarray(ann)

# Normalize the RGB values to the 0-255 range
ann = (ann * 255).astype(np.uint8)

# Combine the RGB values into a single int32 value
int_value = (ann[0] << 16) + (ann[1] << 8) + ann[2]

# Display the result
print("RGB values:", ann)
print("Integer value:", int_value)

# Function to retrieve RGBA values from a 32-bit integer
def int32_to_rgba(int32_value):
    r = (int32_value >> 24) & 0xFF
    g = (int32_value >> 16) & 0xFF
    b = (int32_value >> 8) & 0xFF
    a = int32_value & 0xFF
    return np.array([r, g, b, a]) / 255

# Retrieve the RGB values
retrieved_rgb = int32_to_rgb(int_value)

# Display the result
ann, int_value, retrieved_rgb"""
"""
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

# Define features and target
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, stratify=df['Preferred_Clothing'])

# Apply a Random Forest Classifier
rf_model = RandomForestClassifier(n_estimators=100, random_state=42)
rf_model.fit(X_train, y_train)

# Make predictions
y_pred = rf_model.predict(X_test)

# Compute the confusion matrix
cm = confusion_matrix(y_test, y_pred)
tn, fp, fn, tp = cm.ravel()

# Print true positives, false positives, true negatives, false negatives
print(f"True Positives (TP): {tp}")
print(f"False Positives (FP): {fp}")
print(f"True Negatives (TN): {tn}")
print(f"False Negatives (FN): {fn}")

# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print(f"\nAccuracy: {accuracy}\n")
print("Classification Report:")
print(classification_report(y_test, y_pred))
"""


import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
y = y - 1
# Assuming X and y are your features and target variable
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

model = xgb.XGBClassifier(objective='binary:logistic', learning_rate=0.1,
                          max_depth=5, n_estimators=100)
model.fit(X_train, y_train)

# Make predictions
predictions = model.predict(X_test)

# Evaluate the model
accuracy = accuracy_score(y_test, predictions)
print("Accuracy: %.2f%%" % (accuracy * 100.0))

cm = confusion_matrix(y_test, predictions)
print(cm)

tn, fp, fn, tp = cm.ravel()

# Print true positives, false positives, true negatives, false negatives
print(f"True Positives (TP): {tp}")
print(f"False Positives (FP): {fp}")
print(f"True Negatives (TN): {tn}")
print(f"False Negatives (FN): {fn}")
"""
import lightgbm as lgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create the LightGBM data containers
train_data = lgb.Dataset(X_train, label=y_train)
test_data = lgb.Dataset(X_test, label=y_test, reference=train_data)

# Parameters can be tuned as necessary
parameters = {
    'objective': 'binary',
    'metric': 'binary_logloss',
    'is_unbalance': 'true',
    'boosting': 'gbdt',
    'num_leaves': 31,
    'feature_fraction': 0.5,
    'bagging_fraction': 0.5,
    'bagging_freq': 20,
    'learning_rate': 0.05,
    'verbose': 0
}

model = lgb.train(parameters,
                  train_data,
                  valid_sets=test_data,
                  num_boost_round=500,
                  early_stopping_rounds=50)

predictions = model.predict(X_test)
# Convert probabilities into binary output
predicted_labels = (predictions >= 0.5).astype(int)
accuracy = accuracy_score(y_test, predicted_labels)
print("Accuracy: %.2f%%" % (accuracy * 100.0))

from catboost import CatBoostClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

model = CatBoostClassifier(iterations=100,
                           learning_rate=0.1,
                           depth=5)

# Train the model
model.fit(X_train, y_train, verbose=False)

# Make predictions
predictions = model.predict(X_test)

# Evaluate the model
accuracy = accuracy_score(y_test, predictions)
print("Accuracy: %.2f%%" % (accuracy * 100.0))
"""
# Import necessary libraries
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score

# Assuming X and y are your datasets
# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
y_train = y_train.ravel()
# Create the SVM model using a linear kernel
model = svm.SVC(kernel='linear')

# Train the model with the training data
model.fit(X_train, y_train)

# Make predictions using the testing set
y_pred = model.predict(X_test)

# Output the classification report and accuracy of the model
print(classification_report(y_test, y_pred))
print("Accuracy:", accuracy_score(y_test, y_pred))

